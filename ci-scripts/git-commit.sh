#!/bin/bash

# Commit back to the repository
# version number, push the tag back to the remote.

set -e

# Tag and push
tag=$(semversioner current-version)
if [[ "${BITBUCKET_BRANCH}" =~ "v-" ]]; then
    tag="${tag}-v-${BITBUCKET_BUILD_NUMBER}"
fi

echo $PRIVATE_KEY > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

git add .
git commit -m "Update files for new version '${tag}' [skip ci]"
GIT_SSH_COMMAND="ssh -i ~/.ssh/id_rsa -F /dev/null" git push origin ${BITBUCKET_BRANCH}
