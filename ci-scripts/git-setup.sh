#!/usr/bin/env bash
#
# Configure git to allow push back to the remote repository.
#
# Required globals:
#   PRIVATE_KEY: base64 encoded ssh private key
#

set -e

echo $PRIVATE_KEY > ~/.ssh/id_rsa
echo $PUBLIC_KEY > ~/.ssh/id_rsa.pub
chmod 600 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa.pub
# Configure git
git config user.name $GIT_USER
git config user.email $GIT_MAIL
git remote set-url origin git@bitbucket.org:${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}.git
