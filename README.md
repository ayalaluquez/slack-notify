# Bitbucket Pipelines Pipe: Slack Notify

Sends a custom notification to [Slack](https://slack.com).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe:  ayalaluquez/slack-notify:master
  variables:
    WEBHOOK_URL: '<string>'
    MESSAGE: '<string>'
    URL_SITE: '<string>'
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable           | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| WEBHOOK_URL (*) | Incoming Webhook URL. It is recommended to use a secure repository variable.  |
| MESSAGE (*)     | Notification message. |
| URL_SITE        | site url, returns the status code of the site you are deploying |
| DEBUG           | Turn on extra debug information. Default: `false`. | 

_(*) = required variable._

## Prerequisites

To send notifications to Slack, you need an Incoming Webhook URL. You can follow the instructions [here](https://api.slack.com/incoming-webhooks) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: ayalaluquez/slack-notify:master
    variables:
      WEBHOOK_URL: $WEBHOOK_URL
      MESSAGE: 'Hello, world!'
      URL_SITE: example.com
```

