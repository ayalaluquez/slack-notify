#!/usr/bin/env bash
#
# Send a notification to Slack, https://api.slack.com/docs/messages
#
# Required globals:
#  WEBHOOK_TOKEN
#  MESSAGE

source "$(dirname "$0")/common.sh"

extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  extra_args="--verbose"
fi

URL_SITE=${URL_SITE}

CHECK_HTTP="$(curl -I $URL_SITE |grep HTTP)"


# mandatory variables
WEBHOOK_URL=${WEBHOOK_URL:?'WEBHOOK_URL variable missing.'}


info "Enviando notificación slack..."

curl_output_file="/tmp/pipe-$RANDOM.txt"

payload=$(jq -n \
  --arg MESSAGE "${MESSAGE}" \
  --arg URL_SITE "${URL_SITE}" \
  --arg CHECK_HTTP "${CHECK_HTTP}" \
  --arg BITBUCKET_WORKSPACE "${BITBUCKET_WORKSPACE}" \
  --arg BITBUCKET_REPO_SLUG "${BITBUCKET_REPO_SLUG}" \
  --arg BITBUCKET_BUILD_NUMBER "${BITBUCKET_BUILD_NUMBER}" \
  --arg BITBUCKET_COMMIT "${BITBUCKET_COMMIT}" \
  --arg BITBUCKET_BRANCH "${BITBUCKET_BRANCH}" \
'{ attachments: [
  {
    "fallback": $MESSAGE,
    "color": "#439FE0",
    "pretext": $MESSAGE,
    "footer": "<https://\($URL_SITE)>",
    "ts": "Math.floor(Date.now() / 1000)",
    "fields": [
        {
          "title": "Repositorio",
          "value": "<https://bitbucket.org/\($BITBUCKET_WORKSPACE)/\($BITBUCKET_REPO_SLUG)/|\($BITBUCKET_REPO_SLUG)>",
          "short": true
        },      
        {
          "title": "Ref",
          "value": $BITBUCKET_BRANCH,
          "short": true
        },
        {
          "title": "Check http",
          "value": $CHECK_HTTP,
          "short": true
        } 
      ],
          "actions": [ 
         {
            "type": "button",
            "text": "Pipeline",
            "url": "https://bitbucket.org/\($BITBUCKET_WORKSPACE)/\($BITBUCKET_REPO_SLUG)/addon/pipelines/home#!/results/\($BITBUCKET_BUILD_NUMBER)/" 
          },     
          {
            "type": "button",
            "text": "Commit",
            "url": "https://bitbucket.org/\($BITBUCKET_WORKSPACE)/\($BITBUCKET_REPO_SLUG)/commits/\($BITBUCKET_COMMIT)/" 
          }           
      ]               
  }
]}')

run curl -s -X POST --output ${curl_output_file} -w "%{http_code}" \
  -H "Content-Type: application/json" \
  -d "${payload}" \
  ${extra_args} \
  ${WEBHOOK_URL}

response=$(cat ${curl_output_file})
info "HTTP Response: $(echo ${response} ${CHECK_HTTP})"

if [[ "${response}" = "ok" ]]; then
  success "Notification successful."
else
  fail "Notification failed."
fi
