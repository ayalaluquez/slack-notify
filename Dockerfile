FROM alpine:3.9

RUN apk --no-cache add \
    bash=4.4.19-r1 \
    curl=7.64.0-r3 \
    jq=1.6-r0

COPY common.sh /

COPY pipe /
COPY pipe.yml /

ENTRYPOINT ["/pipe.sh"]
